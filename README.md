# Checkout Full stack challenge

Hello, my name is Kenny Alvizuris and this repo contains my to the checkout technical challenge.

# Table of Contents

1. [How to run the app](#how-to-run)
2. [Future works](#future-works)
3. [Thanks](#thanks)

## How to run ?

1. First clone the project

```sh
git clone git@gitlab.com:Kenny407/checkout.git
```

2. Run the database

```sh
# verbose mode
docker compose up
# or silent mode
docker compose up -d
```

3. Now that the database is running

You can start the api, this will create the database and tables needed for the API to work

```sh
cd api
npm install
npm run dev
```

4. Now time to run the app 🚀

In a different terminal launch the app by going to the `app` directory

```sh
cd app
npm install
npm run start
```

5. The app should be up and running

The API will run in the port 3001 and the app in the port 3000

## Future works

I identified a few things that I'd have loved to improve before handing over the test but I wanted to keep the time frame reasonable.

### App

- Implement calls with API design (hits, hitsPerPage, nbPages, etc)
- Error handling for the request
- Frontend testing

### API

- more testing in the backend functions
- Allow to create users and associate them to cards
- Which would allow us to create a nice login

### Overall improvements

- dockerize the APP and the API: specially to avoid running them in separate commands (steps: 3 and 4)

## Thanks

If you have made it so far, thank you for reading 🎉
Here's my [Github](https://github.com/Kenny407) if you wanna take a look :)
