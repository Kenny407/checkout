import type { Knex } from 'knex'

export const up = async (db: Knex): Promise<void> => {
  await db.schema.createTable('cards', (table) => {
    table.string('id', 255).primary()
    table.string('number', 16).notNullable()
    table.string('holderName', 28).notNullable()
    table.string('expirationDate', 4).notNullable()
    table.string('validationCode', 3).notNullable()
    table.dateTime('createdAt').defaultTo(db.fn.now()).notNullable()
  })

  await db.schema.createTable('users', (table) => {
    table.string('id', 255).primary()
    table.string('firstName', 255).notNullable()
    table.string('lastName', 255).notNullable()
    table.date('birthDate')
  })

  await db.schema.createTable('emails', (table) => {
    table.string('id', 255).primary()
    table.string('userId', 255).references('users.id').notNullable()
  })

  // TODO: Future works: link cards to user
  // await db.schema.alterTable('cards', (table) => {
  //   table.string('userId', 255).references('users.id').notNullable()
  // })
}

export const down = async (db: Knex): Promise<void> => {
  await db.schema.dropTableIfExists('cards')
  await db.schema.dropTableIfExists('emails')
  await db.schema.dropTableIfExists('users')
}
