import knex from 'knex'
import once from 'lodash'
import path from 'path'

import {
  DATABASE_CLIENT,
  DATABASE_POOL_MAX,
  DATABASE_POOL_MIN,
  DATABASE_URL,
  Env,
  NODE_ENV,
} from 'src/env'

export const createDb = () =>
  knex({
    client: DATABASE_CLIENT,
    connection: {
      filename: DATABASE_URL,
      connectionString: DATABASE_URL,
      // disable SSL in dev, but mandatory for other environments
      ssl: NODE_ENV === Env.DEV ? false : { rejectUnauthorized: false },
    },
    pool: {
      min: DATABASE_POOL_MIN,
      max: DATABASE_POOL_MAX,
    },
    log: {
      // TODO: use a robust logger with different levels
      // such as winston
      debug: (message: string) => {
        console.log(`[Knex] Debug: ${message}`)
      },
      deprecate: (message: string) => {
        console.log(`[Knex] Deprecate: ${message}`)
      },
      error: (message: string) => {
        console.log(`[Knex] Error: ${message}`)
      },
      warn: (message: string) => {
        console.log(`[Knex] Warn: ${message}`)
      },
    },
  })

export const dbConnection = createDb()

export const initDatabase = async (runMigrations = true) => {
  console.log('Initializing database...')
  if (runMigrations) {
    console.log('Running migrations')
    await dbConnection.migrate.latest({
      directory: path.join(__dirname, 'migrations'),
    })
    console.log('Database migrated')
  }
}
