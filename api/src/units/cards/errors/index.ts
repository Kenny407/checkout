export class InvalidCardPayloadError extends Error {
  constructor() {
    super('Error validating card payload')
  }
}
