import Koa from 'koa'
import HttpStatusCodes from 'http-status-codes'
import KoaRouter from 'koa-router'
import { middleware } from 'koa-pagination'
import { createNewCard, listCardsEndpoint } from '../actions'
import { InvalidCardPayloadError } from '../errors'

const CARDS_PREFIX = '/cards'

interface ContextWithPagination extends Koa.Context {
  pagination: {
    length: number
    limit: number
    offset: number
  }
}

export const cardsRouter = new KoaRouter({
  prefix: CARDS_PREFIX,
})

cardsRouter.get('/', middleware(), async (ctx: ContextWithPagination, next) => {
  try {
    const result = await listCardsEndpoint({
      limit: ctx.pagination.limit,
      offset: ctx.pagination.offset,
    })

    ctx.status = HttpStatusCodes.OK
    ctx.body = { ...result }
    ctx.pagination.length = result.total
  } catch (error) {
    console.log(`Error while calling [GET] ${CARDS_PREFIX} `, error)
    ctx.status = 500
  }
  await next()
})

cardsRouter.post('/', async (ctx, next) => {
  const { holderName } = ctx.request.body

  try {
    const newCard = await createNewCard({ holderName })
    ctx.status = HttpStatusCodes.OK
    ctx.response.body = newCard
  } catch (error) {
    if (error instanceof InvalidCardPayloadError) {
      ctx.status = HttpStatusCodes.BAD_REQUEST
    } else {
      ctx.status = HttpStatusCodes.INTERNAL_SERVER_ERROR
    }
  }

  await next()
})
