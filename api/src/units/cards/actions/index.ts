import Joi from 'joi'

import { dbConnection } from 'src/database'
import { CustomPagination } from 'src/koa/types'
import {
  generateCardNumber,
  generateExpirationDate,
  generateId,
  generateVerificationCode,
} from 'src/utils/id'
import { sanitizeString } from 'src/utils/sanitize'
import { InvalidCardPayloadError } from '../errors'
import { CardSchema } from '../types'

export const listCards = async () => {
  return dbConnection<CardSchema>('cards').select(
    'holderName',
    'number',
    'expirationDate',
    'validationCode'
  )
}

export const listCardsEndpoint = async (params: CustomPagination) => {
  // TODO: update "magic numbers" on env vars
  const page = params.page ?? 0
  const hitsPerPage = params.hitsPerPage ?? 20

  const cards = await listCards()

  return {
    page,
    hitsPerPage,
    nbPages: Math.ceil(cards.length / hitsPerPage),
    total: cards.length,
    hits: cards.slice(page * hitsPerPage, (page + 1) * hitsPerPage),
  }
}

export const createNewCard = async ({ holderName }: { holderName: string }) => {
  const payload = {
    holderName: sanitizeString(holderName),
  }

  const payloadSchema = Joi.object().keys({
    holderName: Joi.string().max(28),
  })

  try {
    await payloadSchema.validateAsync(payload)
  } catch (error) {
    if (error instanceof Joi.ValidationError) {
      console.log('Error while validating payload for card', error)
      throw new InvalidCardPayloadError()
    }
    console.error('Unexpected error validating payload', error)
    throw error
  }

  const cardPayload = {
    id: generateId(),
    holderName: payload.holderName,
    validationCode: generateVerificationCode(),
    expirationDate: generateExpirationDate(),
    number: generateCardNumber(),
  }
  try {
    await dbConnection<CardSchema>('cards').insert(cardPayload)
  } catch (error) {
    console.log('Error while inserting to DB', error)
    throw error
  }

  return cardPayload
}
