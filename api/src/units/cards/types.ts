export type CardSchema = {
  id: string
  number: string
  holderName: string
  expirationDate: string
  validationCode: string
}
