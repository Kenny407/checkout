export type CustomPagination = {
  limit: number
  offset: number
  page?: number
  hitsPerPage?: number
}
