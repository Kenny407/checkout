import Koa from 'koa'
import json from 'koa-json'
import logger from 'koa-logger'
import Router from 'koa-router'
import koaBodyParser from 'koa-bodyparser'
import { errorMiddleware } from './middlewares/error'
import { cardsRouter } from 'src/units/cards/router'

export const createServer = () => {
  const app = new Koa()
  const router = new Router()

  // app middlewares
  app.use(json())
  app.use(koaBodyParser())
  app.use(errorMiddleware)
  app.use(logger())
  app.use(cardsRouter.routes()).use(cardsRouter.allowedMethods())
  app.use(router.routes()).use(router.allowedMethods())

  // basic get to test API
  router.get('/', async (ctx, next) => {
    ctx.body = { msg: 'hello - api is working' }

    await next()
  })

  return { app }
}
