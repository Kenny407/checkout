import Koa from 'koa'

export const errorMiddleware = async (ctx: Koa.Context, next: Koa.Next) => {
  try {
    await next()
  } catch (err) {
    ctx.status = err?.status || 500
    console.error('Server middleware error', { error: err as Error })
    ctx.body = (err as Error).message
    ctx.app.emit('error', err, ctx)
  }
}
