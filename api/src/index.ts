import { initDatabase } from './database'
import { API_PORT } from './env'
import { createServer } from './koa'

const runServer = async () => {
  try {
    await initDatabase(true)
  } catch (error) {
    console.error('Error while starting database', error)
  }

  const { app } = await createServer()
  try {
    app.listen(API_PORT, () => {
      console.log(`Server running on port ${API_PORT}`)
    })
  } catch (error) {
    console.error('Could not start server', { error })
  }
}

runServer().catch((error: Error) =>
  console.error('Could not initialize server', { error })
)
