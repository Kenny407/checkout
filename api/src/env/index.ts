/* eslint-disable no-process-env */

export enum Env {
  PROD = 'production',
  DEV = 'development',
  TEST = 'test',
  STAGING = 'staging',
}

export const API_PORT = Number(process.env.API_PORT)
export const NODE_ENV: Env = process.env.NODE_ENV as Env

// database variables
export const DATABASE_CLIENT = process.env.DATABASE_CLIENT as string
export const DATABASE_URL = process.env.DATABASE_URL as string
export const DATABASE_POOL_MIN = Number(process.env.DATABASE_POOL_MIN)
export const DATABASE_POOL_MAX = Number(process.env.DATABASE_POOL_MAX)
