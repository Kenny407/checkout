import { generateCardNumber, generateVerificationCode } from './id'

describe('ids generator', () => {
  describe('generateCardNumber', () => {
    test('should return a 16 letter card', () => {
      expect(generateCardNumber().length).toEqual(16)
    })

    test('should return a different id', () => {
      const cardNumber = generateCardNumber()
      expect(cardNumber).not.toEqual(generateCardNumber)
    })
  })

  describe('generateVerificationCode', () => {
    test('should return a 3 digit verification code', () => {
      expect(generateVerificationCode().length).toEqual(3)
    })

    test('should return a different id', () => {
      const verificationCode = generateVerificationCode()
      expect(verificationCode).not.toEqual(generateVerificationCode())
    })
  })
})
