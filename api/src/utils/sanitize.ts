import { lowerCase, deburr, trim } from 'lodash/fp'

export const sanitizeString = (str: string) => lowerCase(trim(deburr(str)))
