import { customAlphabet } from 'nanoid'

const ID_LENGTH = 255
const CARD_NUMBER_LENGTH = 16
const VERIFICATION_CODE_LENGTH = 3

const ALPHANUMERIC_ALPHABET =
  'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
const NUMERICAL_ALPHABET = '0123456789'

export const generateCardNumber = customAlphabet(
  NUMERICAL_ALPHABET,
  CARD_NUMBER_LENGTH
)

export const generateVerificationCode = customAlphabet(
  NUMERICAL_ALPHABET,
  VERIFICATION_CODE_LENGTH
)

export const generateRandomMonthNumber = () => {
  return String(Math.floor(Math.random() * (12 - 1) + 1))
}

export const generateId = customAlphabet(ALPHANUMERIC_ALPHABET, ID_LENGTH)

export const generateExpirationDate = () => {
  const today = new Date()
  const futureYear = Number(today.getFullYear().toString().substring(2, 4)) + 3
  const randomMonth = generateRandomMonthNumber()

  return `${randomMonth.padStart(2, '0')}${futureYear}`
}
