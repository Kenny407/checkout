module.exports = {
  preset: 'ts-jest',
  roots: ['src'],
  setupFilesAfterEnv: ['<rootDir>/jestSetup.js'],
}
