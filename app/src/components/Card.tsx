import { Flex, Spacer, Text, useColorModeValue } from "@chakra-ui/react";
import { FC, useState } from "react";

interface Props {
  holderName: string;
  cardNumber: number;
  cvv: number;
  expiredAt: string;
}

const Card: FC<Props> = ({ holderName, cardNumber, cvv, expiredAt }) => {
  const [isMasked, setIsMasked] = useState(true);

  const cardNumberPretty = (`${cardNumber}`.match(/.{1,4}/g) || []).join(" ");
  const last4Digits = cardNumberPretty.split(" ").pop();
  const maskedCard = `**** **** **** ${last4Digits}`;
  const month = expiredAt.substring(0, 2);
  const year = expiredAt.substring(2, expiredAt.length);

  return (
    <Flex
      shrink="0"
      bg={useColorModeValue("gray.50", "blackAlpha.400")}
      width="300px"
      height="190px"
      borderRadius="10px"
      p={4}
      direction="column"
      justifyContent="flex-end"
      fontFamily="monospace"
      gap={2}
      boxShadow="md"
    >
      <Text fontSize={24}>Checkout</Text>
      <Spacer />
      <Text
        fontSize={22}
        cursor="pointer"
        onClick={() => setIsMasked(!isMasked)}
      >
        {isMasked ? maskedCard : cardNumberPretty}
      </Text>
      <Flex gap={4}>
        <Flex direction="column">
          <Text fontWeight="bold">Valid Thru</Text>
          <Text>
            {month} / {year}
          </Text>
        </Flex>
        <Flex direction="column">
          <Text fontWeight="bold">CVV</Text>
          <Text>{cvv}</Text>
        </Flex>
      </Flex>
      <Text>{holderName}</Text>
    </Flex>
  );
};

export default Card;
