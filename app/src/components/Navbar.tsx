import { Flex, Container, Spacer, useColorModeValue } from "@chakra-ui/react";
import { FC } from "react";

import Logo from "components/Logo";
import { ColorModeSwitcher } from "components/ColorModeSwitcher";

interface Props {}

const Navbar: FC<Props> = () => {
  const bg = useColorModeValue("white", "gray.900");

  return (
    <Flex
      w="100vw"
      bg={bg}
      boxShadow="md"
      p={1}
      borderBottom="2px"
      borderColor="#00122C"
    >
      <Container maxW="6xl" display="flex" alignItems="center">
        <Logo />
        <Spacer />
        <ColorModeSwitcher />
      </Container>
    </Flex>
  );
};

export default Navbar;
