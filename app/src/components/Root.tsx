import { FC } from "react";
import { ChakraProvider, theme } from "@chakra-ui/react";

import { App } from "components/App";

interface Props {}

const Root: FC<Props> = () => {
  return (
    <ChakraProvider theme={theme}>
      <App />
    </ChakraProvider>
  );
};

export default Root;
