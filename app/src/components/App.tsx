import { useEffect, useState } from "react";
import {
  Flex,
  Container,
  Heading,
  Button,
  Input,
  Divider,
} from "@chakra-ui/react";

import Card from "components/Card";
import Navbar from "components/Navbar";

interface CreditCard {
  id: string;
  holderName: string;
  validationCode: number;
  expirationDate: string;
  number: number;
}

export const App = () => {
  const [cardHolderName, setCardHolderName] = useState<string>("");
  const [cards, setCards] = useState<CreditCard[]>([]);

  useEffect(() => {
    const getCards = async () => {
      const res = await fetch("/cards");
      const { hits: cards } = await res.json();
      // TODO: Add error handling
      setCards(cards);
    };
    getCards();
  }, []);

  const onClickGenerateCard = async () => {
    const payload = {
      holderName: cardHolderName,
    };

    const res = await fetch("/cards", {
      headers: { "Content-Type": "application/json" },
      method: "POST",
      body: JSON.stringify(payload),
    });

    const card = await res.json();
    // TODO: Add error handling
    setCards([card].concat(cards));
    setCardHolderName("");
  };

  return (
    <>
      <Navbar />
      <Container
        maxW="6xl"
        p={3}
        display="flex"
        flexDir="column"
        gap={4}
        alignItems="flex-start"
      >
        <Heading>
          <span>👋🏼</span> Hello, let's create a new payment card!
        </Heading>

        <Flex gap={2}>
          <Input
            placeholder="Cardholder's Name"
            minW={400}
            value={cardHolderName}
            onChange={(e: any) => setCardHolderName(e.target.value)}
          />
          <Button
            isDisabled={cardHolderName.length < 2 || cardHolderName.length > 28}
            onClick={onClickGenerateCard}
            boxShadow="sm"
            leftIcon={<span>💳</span>}
            flexShrink={0}
          >
            Generate new card
          </Button>
        </Flex>

        <Divider pt={4} />

        <>
          <Heading>
            Your cards <span>💳</span>
          </Heading>
          <Flex
            gap={3}
            overflow="auto"
            width="100%"
            py={6}
            px={4}
            boxShadow="0px 0px 50px -40px gray inset"
            minH="238px"
          >
            {cards.map((card) => (
              <Card
                key={card.id}
                holderName={card.holderName}
                cardNumber={card.number}
                cvv={card.validationCode}
                expiredAt={card.expirationDate}
              />
            ))}
          </Flex>
        </>
      </Container>
    </>
  );
};
